# Soft Marvel Plan

Soft Marvel Plan é um projeto desenvolvido para a realização do desafio técnico enviado pela Softplan.

## Como contribuir

Para contribuir com o projeto é preciso fazer o clone do repositório:

```shell
git clone git@gitlab.com:davi-faustino/soft-marvel-plan.git # SSH
git clone http://gitlab.com/davi-faustino/soft-marvel-plan.git # HTTP
```

Em seguida use `yarn` ou `yarn install` para instalar as dependências, depois execute `yarn start` para incializar a aplicação no navegador.

## Boas práticas

* Sempre realize os testes automatizados e verifique o coverage usando `yarn test`.
* Tente manter o `código limpo`.
* Escreva tudo o que foi feito ao abrir o `merge request`.
* Sempre que mexer em um código, tente melhora-lo.
