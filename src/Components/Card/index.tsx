import React, { useState } from 'react';
import { Image } from '../../styles/GlobalStyle';
import {
  PersonagemCard,
  Title,
  Description,
  ContainerDetalhes,
  TextoDetalhes
} from './styles';

export default function MyCard(props:PersonagemCard) {
  const [ mouseEnter, setMouseEnter ] = useState(false);

  return (
    <PersonagemCard
      hasDetalhes={props.hasDetalhes}
      onMouseEnter={() => setMouseEnter(true)}
      onMouseLeave={() => setMouseEnter(false)}
    >
      {(mouseEnter && props.hasDetalhes) && (
        <ContainerDetalhes onClick={props.verDetalhes}>
          <TextoDetalhes>Ver detalhes</TextoDetalhes>
        </ContainerDetalhes>
      )}
      <Image src={props.thumbnail} preview={false} />
      <Title>{props.name}</Title>
      <Description cortarDescricao={props.cortarDescricao}>
        {props.description || 'sem descrição'}
      </Description>
    </PersonagemCard>
  )
}