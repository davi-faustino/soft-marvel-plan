import styled from 'styled-components';

export const PersonagemCard = styled.div<PersonagemCardInt>`
  display: flex;
  flex-direction: column;
  max-width: 220px;
  padding: 10px;
  border-radius: 10px;
  height: 100%;
  background: #524F4F;
  filter: drop-shadow(-5px 4px 8px rgba(0, 0, 0, 0.3));
  transition: transform 500ms;
  position: relative;
  
  ${props => props.hasDetalhes && `
    :hover {
      filter: drop-shadow(2px 6px 24px rgba(0, 0, 0, 0.9));
      z-index: 2;
      transform: scale(1.2)
    }
  `}
`;

export const Title = styled.span`
  font-size: 20px;
  font-weight: bold;
  line-height: 25px;
  word-wrap: break-all;
  padding: 10px 0;
  text-align: initial;
`;

export const Description = styled.span<PropsDescription>`
  font-style: normal;
  font-size: 14px;
  text-align: initial;

  ${props => props.cortarDescricao && `
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  `}
`;

export const ContainerDetalhes = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: 10px;
  border: none;
  background-color: #1d1c1cb8;
  cursor: pointer;
`;

export const TextoDetalhes = styled.span`
  font-size: 25px;
  text-decoration: underline #8B8989 1px;
`;