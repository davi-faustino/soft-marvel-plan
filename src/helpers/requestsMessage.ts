import { notification } from "antd";

export const errorRequestPersonagens = () =>
  notification.error({
    message: "Erro",
    description: "Erro ao buscar os personagens, desculpe pelo transtorno.",
  });

export const errorRequestSeries = () =>
  notification.error({
    message: "Erro",
    description:
      "Erro ao buscar as séries do personagem, desculpe pelo transtorno.",
  });
