import { makeVar } from "@apollo/client";

export const detalhesPersonagem = makeVar<Personagem>({} as Personagem);

export const showModal = makeVar(false);
