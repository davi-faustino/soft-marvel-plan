export const sleep = async (time = 2000) =>
  new Promise((r) => setTimeout(r, time));

describe("utils thread", () => {
  it("fake test para validar o arquivo utils thread", () => {
    expect(1 + 1).toBe(2);
  });
});
