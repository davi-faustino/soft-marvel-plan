import React from "react";
import { render, fireEvent, act } from '@testing-library/react'
import Card from '../../../Components/Card';

describe('Realiza teste do componente Card', () => {
  const onClickCard = jest.fn();

  const props:PersonagemCard = {
    thumbnail: 'teste.png',
    name: 'Teste',
    description: 'Realizando teste automatizado',
    hasDetalhes: true,
    cortarDescricao: true,
    verDetalhes: onClickCard
  };

  it("Renderiza o card com informações", () => {
    const { getByText } = render(
      <Card {...props} />
    );

    const cardName = getByText(props.name);
    const cardDescription = getByText(props.description);

    expect(cardName).toBeTruthy();
    expect(cardDescription).toBeTruthy();
  });

  it("Caso o card não possua descrição, renderiza o card com uma descrição padrão", () => {
    const { getByText } = render(
      <Card {...props} description="" />
    );

    const descricaoPadrao = getByText('sem descrição');

    expect(descricaoPadrao).toBeTruthy();
  });

  it("Ao clicar no card, chama a função responsável por ver os detalhes do personagem", async () => {
    const { getByText } = render(
      <Card {...props} />
    );
    const cardName = getByText(props.name);
    await act(async () => {
      fireEvent.mouseOver(cardName);
    })
    
    const verDetalhes = getByText('Ver detalhes');
    fireEvent.click(verDetalhes);

    expect(onClickCard).toHaveBeenCalled();
  });

  it("Ao remover o mouse de cima do card, deve sumir o botão de ver detalhes", async () => {
    const { getByText, queryByText } = render(
      <Card {...props} />
    );
    
    const cardName = getByText(props.name);
    await act(async () => {
      fireEvent.mouseLeave(cardName);
    })
    
    const verDetalhes = queryByText('Ver detalhes');
    expect(verDetalhes).toBeFalsy();
  });
})