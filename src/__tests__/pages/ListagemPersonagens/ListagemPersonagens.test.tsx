import React from "react";
import { render, act } from '@testing-library/react'
import { MockedProvider } from '@apollo/client/testing';
import { sleep } from "../../utils/thread";
import { getPersonagens } from "../../../graphql";
import ListagemPersonagens from '../../../pages/ListagemPersonagens';

describe('Realiza teste da página ListagemPersonagens', () => {
  const getPersonagensData = {
    data: {
      characters: [{
        id: "1011334",
        name: "3-D Man",
        description: "",
        thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg",
      }]
    }
  };
  
  const mocks:any = [
    {
      request: {
        query: getPersonagens,
        notifyOnNetworkStatusChange: true
      },
      result: getPersonagensData
    }
  ]

  it("Renderiza a ListagemPersonagens", async () => {
    const { getByText } = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <ListagemPersonagens />
      </MockedProvider>
    );

    await act(async () => {
      await sleep();
    });
    
    const personagem = getPersonagensData.data.characters[0];

    const nomePersonagem = getByText(personagem.name)

    expect(nomePersonagem).toBeTruthy();
  });

  it("Retorna uma mensagem de erro caso ocorra algum problema durante a requisição", async () => {
    const mockError = [{
      request: {
        query: getPersonagens,
        notifyOnNetworkStatusChange: true
      },
      error: new Error('An error occurred')
    }];
    
    const { getByText } = render(
      <MockedProvider mocks={mockError} addTypename={false}>
        <ListagemPersonagens />
      </MockedProvider>
    );

    await act(async () => {
      await sleep();
    });

    const mensagemErro = getByText('Erro ao buscar os personagens, desculpe pelo transtorno.');

    expect(mensagemErro).toBeTruthy();
  });
})