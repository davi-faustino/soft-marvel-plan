import React from "react";
import { render, act } from '@testing-library/react'
import { MockedProvider } from '@apollo/client/testing';
import { sleep } from "../../utils/thread";
import { getSeriesPersonagem } from "../../../graphql";
import { detalhesPersonagem, showModal } from "../../../reactiveVar";
import DetalhesPersonagem from '../../../pages/DetalhesPersonagem';

describe('Realiza teste da modal DetalhesPersonagem', () => {
  showModal(true);
  detalhesPersonagem({
    id: 1011334,
    name: "3-D Man",
    description: "",
    thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg",
  });

  const getPersonagensData = {
    data: {
      series: [{
        id: 1945,
        title: "Avengers: The Initiative (2007 - 2010)",
        description: null,
        thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/514a2ed3302f5.jpg",
      }]
    }
  };
  
  const mocks:any = [
    {
      request: {
        query: getSeriesPersonagem,
        variables: {
          where: {
            characters: 1011334
          }
        }
      },
      result: getPersonagensData
    }
  ]

  it("Renderiza os detalhes do personagem", async () => {
    const { getByText } = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <DetalhesPersonagem />
      </MockedProvider>
    );

    await act(async () => {
      await sleep();
    });
    
    const nomePersonagem = getByText("3-D Man")

    expect(nomePersonagem).toBeTruthy();
  });
})