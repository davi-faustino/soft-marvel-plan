import "@testing-library/jest-dom";
import "jest-styled-components";
import "@testing-library/jest-dom/extend-expect";

Object.defineProperty(window, "matchMedia", {
  value: () => ({
    matches: false,
    addListener: () => {},
    removeListener: () => {},
  }),
});
