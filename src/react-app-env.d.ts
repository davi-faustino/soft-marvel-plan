/// <reference types="react-scripts" />
interface Personagem {
  id: number;
  name: string;
  thumbnail: string;
  description: string;
}

interface Serie {
  id: number;
  title: string;
  thumbnail: string;
  description: string;
}

interface ResponsePersonagens {
  characters: [Personagem];
}

interface ResponseSeries {
  series: [Serie];
}

interface PersonagemCardInt {
  hasDetalhes: boolean;
}

interface PropsDescription {
  cortarDescricao: boolean;
}

interface propsFieldsValue {
  name: string;
  description: string;
}

interface PersonagemCard {
  thumbnail: string;
  name: string;
  description: string;
  hasDetalhes: boolean;
  cortarDescricao: boolean;
  verDetalhes?: MouseEventHandler<HTMLButtonElement>;
}
