import styled, { createGlobalStyle } from 'styled-components';
import { Image as ImageAntd} from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

export const GlobalStyles = createGlobalStyle`
  body {
    background-color: #313131;
    font-family: Roboto, sans-serif;
    font-style: normal;
  }

  span {
    color: #FFF;
  }

  .inputStyle {
    border-radius: 5px;
    border-color: #4E4D4D;
    background-color: #C4C4C4;
    resize: none;

    :hover, :focus {
      border-color: #4E4D4D;
      box-shadow: none;
    }

    ::placeholder {
      color: #4E4D4D;
    }
  }
`;

export const Image = styled(ImageAntd)`
  border-radius: 10px;
  width: 200px;
  height: 165px;
  object-fit: cover;
`;

export const Loading = styled(LoadingOutlined)`
  margin-top: 10px;
  font-size: 25px;
  color: #C4C4C4;
`;