import 'antd/dist/antd.css';

import { ApolloProvider } from "@apollo/client";
import { apolloClient } from './configuration/apollo';
import Routes from './routes';
import { GlobalStyles } from './styles/GlobalStyle';

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <GlobalStyles />
      <Routes />
    </ApolloProvider>
  );
}

export default App;
