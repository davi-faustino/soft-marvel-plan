import { Route, BrowserRouter } from 'react-router-dom';

import ListagemPersonagens from '../pages/ListagemPersonagens';

export default function Routes() {
  return (
    <BrowserRouter>
      <Route component={ListagemPersonagens} path="/" exact />
    </BrowserRouter>
  );
}