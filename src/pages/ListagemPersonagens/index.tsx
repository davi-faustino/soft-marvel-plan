import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { Row, Col } from 'antd';
import { useQuery, useReactiveVar } from '@apollo/client';
import MyCard from '../../Components/Card';
import { getPersonagens } from '../../graphql'
import { detalhesPersonagem, showModal } from '../../reactiveVar';
import { errorRequestPersonagens } from '../../helpers/requestsMessage';
import DetalhesPersonagem from '../../pages/DetalhesPersonagem';
import { Loading } from '../../styles/GlobalStyle';
import {
  Header,
  HeaderLeft,
  HeaderRight,
  Title,
  Search,
  Container
} from './styles';


export default function ListagemPersonagens() {
  const [ personagens, setPersonagens ] = useState<Personagem[]>([]);
  const [ limit, setLimit] = useState(0);
  const modalVisible = useReactiveVar(showModal);

  const { loading, error, data, fetchMore } = useQuery<ResponsePersonagens>(
    getPersonagens, {
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    setPersonagens(data?.characters || [])
  }, [data])

  const loadMore = async () => {
    const novosPersonagens = await fetchMore({
      variables: {
        limit: limit + 20
      }
    });

    setPersonagens([...personagens, ...novosPersonagens.data.characters])
    setLimit(limit + 20);
  }

  const fetch = async (pesquisa = '') => {
    let options = {};
    
    console.log({a: 'aloooooo', pesquisa});
    if(pesquisa) {
      options = {
        variables: {
          offset: 100,
          where: {
            nameStartsWith: pesquisa
          }
        }
      }
    } else {
      options =  {
        variables: {
          limit
        }
      }
    }

    const novosPersonagens = await fetchMore(options);

    setPersonagens(novosPersonagens.data.characters)
    setLimit(0)
  };

  const verDetalhes = (personagem:Personagem) => {
    detalhesPersonagem(personagem);
    showModal(true);
  }

  return (
    <>
      {error && errorRequestPersonagens()}
      <Header>
        <HeaderLeft className="left">
          <Title>Personagens Marvel</Title>
        </HeaderLeft>

        <HeaderRight className="right" style={{display: 'flex'}}>
          <Search
            allowClear
            placeholder="Pesquisar personagem"
            onSearch={fetch}
          />
        </HeaderRight>
      </Header>
      <Container bordered={false}>
        <InfiniteScroll
          pageStart={0}
          loadMore={loadMore}
          hasMore={true}
          initialLoad={false}
        >
          <Row gutter={[12, 24]}>
            {personagens.map(personagem => (
              <Col key={personagem.id}>
                <MyCard
                  name={personagem.name}
                  description={personagem.description}
                  thumbnail={personagem.thumbnail}
                  hasDetalhes={true}
                  cortarDescricao={true}
                  verDetalhes={() => verDetalhes(personagem)}
                />
              </Col>
            ))}
          </Row>
        </InfiniteScroll>
        {loading && <Loading spin /> }
      </Container>
      {modalVisible && <DetalhesPersonagem />}
    </>
  )
};