import styled from 'styled-components';
import { Input, Card } from 'antd';

export const Header = styled.div`
  position: fixed;
  top: 0;
  z-index: 3;
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  background-color: #181818;
  color: #FFF;
`;

export const HeaderLeft = styled.div``;

export const HeaderRight = styled.div`
  display: flex;
`;

export const Title = styled.span`
  font-weight: normal;
  font-size: 25px;
  line-height: 70px;
`;

export const Search = styled(Input.Search)`

  .ant-input-search, .ant-input-affix-wrapper {
    border-top-left-radius: 5px !important;
    border-bottom-left-radius: 5px !important;
  }

  .ant-input-group-addon {
    border-top-right-radius: 7px !important;
    border-bottom-right-radius: 7px !important;
  }

  .ant-input-search-button {
    border-top-right-radius: 5px !important;
    border-bottom-right-radius: 5px !important;
  }

  .ant-input-affix-wrapper, .ant-input, .ant-input-search-button {
    border-color: #4E4D4D;
    background-color: #C4C4C4;

    :hover, :focus {
      border-color: #4E4D4D;
      box-shadow: none;
    }
  }

  .ant-input, .ant-input::placeholder {
    color: #4E4D4D;
  }

  .anticon-search > svg {
    fill: black;
  }
`;

export const Container = styled(Card)`
  background-color: transparent;
  padding: 100px 75px 0;
  text-align: center;
`;