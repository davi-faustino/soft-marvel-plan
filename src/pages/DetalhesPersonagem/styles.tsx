import styled from 'styled-components';
import {
  Modal as ModalAntd,
  Input as InputAntd,
  Upload as UploadAntd
} from 'antd';

export const Modal = styled(ModalAntd)`
  width: 100% !important;

  .ant-modal-content {
    background-color: #524F4F;
  }

  .ant-modal-body {
    padding-left: 0;
  }
`;

export const Personagem = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 67px;
  padding-left: 50px;
`;

export const Title = styled.span`
  font-weight: bold;
  font-size: 30px;
`;

export const EspacoSeries = styled.div`
  overflow-y: scroll;
  height: 400px;
  background-color: #303030;
  padding: 15px;
  border-radius: 10px;
  margin-top: 20px;
  text-align: center;
`;

export const Description = styled.span`
  font-size: 14px;
`;

export const Acoes = styled.button`
  background-color: transparent;
  border: none;
  float: right;
  padding-right: 50px;
  cursor: pointer;

  span {
    font-size: 18px;
    color: #FFF
  }

  :hover span{
    color: #303030;
  }
`;

export const Label = styled.span`
  font-weight: bold;
`;

export const Input = styled(InputAntd)`
  
`;

export const Upload = styled(UploadAntd)`
  margin: 30px 0 40px;
`;