import React, { useState } from 'react';
import { CheckOutlined, EditOutlined } from '@ant-design/icons';
import { Row, Col, Form as FormAntd } from 'antd';
import { useQuery, useReactiveVar } from '@apollo/client';
import { getSeriesPersonagem } from '../../graphql';
import MyCard from '../../Components/Card';
import { detalhesPersonagem, showModal } from '../../reactiveVar';
import { errorRequestSeries } from '../../helpers/requestsMessage';
import { getBase64 } from '../../helpers/convertToBase64';
import { apolloClient } from '../../configuration/apollo';
import { Image, Loading } from '../../styles/GlobalStyle';
import {
  Modal,
  Personagem,
  Title,
  EspacoSeries,
  Description,
  Label,
  Acoes,
  Input,
  Upload
} from './styles';

export default function DetalhesPersonagem() {
  const { cache } = apolloClient;
  const [ form ] = FormAntd.useForm();
  const personagem = useReactiveVar(detalhesPersonagem);
  const modalVisible = useReactiveVar(showModal);
  const [ modoEdicao, setModoEdicao ] = useState(false);
  const [ imageUrl, setImageUrl ] = useState('');
  
  const { error, loading, data } = useQuery<ResponseSeries>(
    getSeriesPersonagem,
    {
      variables: {
        where: {
          characters: personagem.id
        }
      }
    }
  );

  const closeModal = () => showModal(false);

  const changeImagem = (info:any) => {
    getBase64(info.file, (imageUrl: string) => {
      setImageUrl(imageUrl)
    });
  };

  const editarPersonagem = async () => {
    if(modoEdicao) {
      const { name, description }:propsFieldsValue = form.getFieldsValue();

      cache.modify({
        id: "Character:".concat(personagem.id.toString()),
        fields: {
          thumbnail() {
            return imageUrl
          },
          name() {
            return name
          },
          description() {
            return description
          },
        }
      })

      detalhesPersonagem({
        id: personagem.id,
        name,
        description,
        thumbnail: imageUrl
      });
    } else {
      setImageUrl(personagem.thumbnail);
      form.setFieldsValue({
        name: personagem.name,
        description: personagem.description
      })
    }
    
    setModoEdicao(!modoEdicao);
  };

  return (
    <>
      {error && errorRequestSeries()}

      <Modal
        visible={modalVisible}
        onCancel={closeModal}
        footer={null}
      >
        <Row gutter={12}>
          <Col span={6}>
            <Acoes onClick={editarPersonagem}>
              {modoEdicao
                ? (
                  <>
                    <Label>Salvar</Label>
                    <CheckOutlined />
                  </>
                ) : (
                  <>
                    <Label>Editar</Label>
                    <EditOutlined />
                  </>
                )
              }
            </Acoes>
            <Personagem>
              {modoEdicao
                ? (
                  <FormAntd 
                    form={form}
                    style={{textAlign: "center"}}
                  >
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      customRequest={changeImagem}
                    >
                      <Image src={imageUrl || personagem.thumbnail} preview={false} />
                    </Upload>
                    <FormAntd.Item name="name">
                      <Input
                        type="text"
                        placeholder="Nome do personagem"
                        className="inputStyle"
                      />
                    </FormAntd.Item>
                    <FormAntd.Item name="description">
                      <Input.TextArea
                        placeholder="Descrição do personagem"
                        className="inputStyle"
                      />
                    </FormAntd.Item>
                  </FormAntd>
                ) : (
                  <>
                    <Image src={personagem.thumbnail} preview={false} />
                    <Title>
                      {personagem.name}
                    </Title>
                    <Description>
                      {personagem.description || 'Sem descrição'}
                    </Description>
                  </>
                )
              }
            </Personagem>
          </Col>
          <Col span={18}>
            <Title>Séries que participou</Title>
            <EspacoSeries>
              {loading && <Loading spin /> }
              <Row gutter={[12, 24]}>
                {data?.series.map(serie => (
                  <Col key={serie.id}>
                    <MyCard
                      name={serie.title}
                      description={serie.description}
                      thumbnail={serie.thumbnail}
                      hasDetalhes={false}
                      cortarDescricao={false}
                    />
                  </Col>
                ))}
              </Row>
            </EspacoSeries>
          </Col>
        </Row>
      </Modal>
    </>
  )
};