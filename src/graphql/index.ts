import { gql } from "@apollo/client";

export const getPersonagens = gql`
  query getPersonagens($limit: Int, $offset: Int, $where: CharacterWhereInput) {
    characters(limit: $limit, where: $where, offset: $offset) {
      id
      name
      description
      thumbnail
    }
  }
`;

export const getDetalhesPersonagem = gql`
  query getDetalhesPersonagem($where: CharacterWhereInput) {
    characters(where: $where) {
      id
      name
      description
      thumbnail
    }
  }
`;

export const getSeriesPersonagem = gql`
  query getSeriesPersonagem($where: SeriesWhereInput) {
    series(where: $where) {
      id
      title
      description
      thumbnail
    }
  }
`;

export const atualizarPersonagem = gql`
  mutation atualizarPersonagem($name: String!, $description: String!) {
    atualizarPersonagem(data: { name: $name, description: $description }) {
      id
      name
      description
      thumbnail
    }
  }
`;
